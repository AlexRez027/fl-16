function visitLink(path) {
	if (!localStorage[path]) {
		window.localStorage.setItem(path, 0)
	}
	for (const key in localStorage) {
		if (Object.hasOwnProperty.call(localStorage, key)) {
			if (key === path) {
				localStorage[key] = +localStorage[key] + 1
			}
		}
	}
}

function viewResults() {
	let h = '<ul>'
	for (const key in localStorage) {
		if (Object.hasOwnProperty.call(localStorage, key)) {
			h = h + `<li>You visited ${key} ${localStorage[key]} time(s)</li>`
		}
	}
	h = h + '</ul>'
	document.querySelector('.mx-auto').insertAdjacentHTML('beforeend', h)
	localStorage.clear()
	//your code goes here
}
