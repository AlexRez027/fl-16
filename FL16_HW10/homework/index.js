//=====Task 1
function isEqual(a,b){
    return a===b
}
console.log(isEqual(5,6));

//=====Task 2

function isBigger(a,b){
    return a > b
}
console.log(isBigger(7,6));

//=====Task 3
function storeNames(){
    let ar = [...arguments]
    return ar
}
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

//=====Task 4
function getDifference(a,b) {
    if (a > b){
        return a-b
    }else{
        return b-a
    }
}
console.log(getDifference(50,80));

//====Task #5
function negativeCount(ar) {
    let count = 0
    for (let i = 0; i < ar.length; i++) {
        if (ar[i] < 0){
            count = count + 1
        }
    }
    return count
}
console.log(negativeCount([5,3,2,3,2,-6,0]));

//====Task #6
function letterCount(string, letter) {
    return string.length - string.replaceAll(letter,'').length
}

console.log(letterCount('Barry','r'));

//====Task #7
function countPoints(points) {
    let count = 0
    for (let i = 0; i < points.length; i++) {
        let match = points[i].split(':')
        if (+match[0] > +match[1]){
            count = count + 3
        }else if (+match[0] === +match[1]){
            count +=1
        }
    }
    return count
}
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));